# Git-hooks

Git hooks repository focused in .Net solution.

## Requirements

 - Be a .Net project integrated with Git as source version control.
 - Git installed in the environment where hooks will run.
 - MSBuild installed in the environment where hooks will run.
 - VSTest.console installed in the environment where hooks will run (That means Visual Studio installed).

## Install Hooks

Copy all files from this repository (except README.md) to a folder in the root path of your .Net project repository.
Go to your .Net project repository in git terminal and execute the follow command.

    $ ./<FOLDER_NAME_WHERE_YOU_PASTED_THE_HOOKS_FILES>/install-hooks.bash

DONE!!!